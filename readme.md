# Documentation de la carte SHIELD Arduino Nano Every

## Introduction

Cette carte a pour objectif de protéger les entrées/sorties de la carte Arduino Nano Every.  Elle met aussi à disposition différentes connectiques afin de simplifier le câblage.  Ses principaux avantages sont :

- Compatible avec les Arduino Nano
- Laisse accessible les tensions 3,3V et 5V
- 2 boutons poussoirs pour pouvoir déclencher des actions
- Borniers à visser pour des connexions faciles et fiables
- Prise en charge du driver moteur DRI0044

## Avertissement

En cas d'inversion de polarité lors du raccordement de la batterie, le fusible devra sûrement être changé. Veuillez aussi contrôler l'intégrité de la diode associée.

## Valeurs maximales

**!!! Veillez à ne jamais dépasser ces valeurs !!!**

| Nom  | Définition          | Valeur | Unité |
| :--------------- |:---------------| :-----| :-----|
| $V_{bat}$ (1) |   Tension d'alimentation de la carte        |  12 |V |
| $V_{PIN}$  | Tension injectée dans les différents connecteurs             |   5 |V |
| $I_{max}$ (2) | Courant maximal que la carte peut consommer (avec moteurs)          |    10 |A |

(1) : Cette limite provient de la tension maximale du driver DRI0044

(2) : Le courant maximal que la carte Arduino Nano Every peut consommer est 1 A. Le reste du courant consommé est en fonction du driver moteur associé.

## Connexions

![Emplacement des connecteurs](img/connecteurs.png)

Les principaux composants de la carte sont :

- En gris, l'emplacement de l'Arduino Nano Every
- En violet, les bornes permettant d'alimenter la carte
- En bleu clair, les bornes permettant d'accéder au 5V ou 3,3V
- En bleu foncé, les connecteurs à 3 broches pour des capteurs binaires
- En jaune, les boutons-poussoirs
- En brun, les jumpers permettant de choisir entre bouton-poussoir et capteur
- En rouge, les bornes permettant d'accéder aux pattes analogiques de l'Arduino
- En orange, le bornier pour les moteurs
- En rose, l'emplacement du driver moteur DRI0044
- En bordeaux, les connecteurs pour les encodeurs moteurs
- En bleu clair, les bornes permettant de fixer la tension de référence de la conversion analogique numérique (CAN)

### L'arduino Nano Every

La carte a été conçue pour l'Arduino Nano Every.  pour plus d'informations sur ce cerier, consultez sa notice :

- https://docs.arduino.cc/resources/datasheets/ABX00028-datasheet.pdf

### Bornier d'alimentation (violet)

La tension d'alimentation est à appliquer dans **l'entrée gauche** du bornier et la masse à sa droite. Une inversion de l'entrée détruira le fusible sur la carte. Il faudra alors contacter votre enseignant.

### Bornes 5V et 3,3V (bleu clair)

Ces bornes servent à alimenter d'autres circuits (actionneurs ou capteurs), conus pour fonctionner en 5V ou 3,3V.

**Attention** : N'essayez pas d'alimenter la carte par ces bornes !

Il y a trois bornes pour le 5V, et une pour le 3V3.  Chacune est accompagnée d'une borne de masse (GND).  
Il est à noter que la tension est **toujours** située à gauche, et la masse à droite.

### Connecteurs 3 broches pour capteurs binaires (bleu foncé)

Ces connecteurs sont prévus pour des capteurs binaires.  Ils ont une borne *+5V* pour alimenter le capteur et une borne *input* pour récupérer le signal du capteur.

Ils sont câblés de la sorte :

![](img/connec03.png)

Les *inputs* sont reliées à l'Arduino de la sorte :

- SENSOR1 = D3
- SENSOR2 = D2
- SENSOR3 = D12
- SENSOR4 = D13

### Boutons poussoirs (j)

Les boutons poussoirs sont câblés de la sorte :

- BUTTON1 = D3
- BUTTON2 = D2

Les boutons poussoirs sont fournis avec un filtre RC passe bas du premier ordre.   filtre permet d'éviter les rebonds.  Il n'est donc pas nécessaire de prévoir un délai logiciel ou un filtre à part pour utiliser ces boutons.  
Il est intéressant de remarquer que les boutons sont câblés sur les mêmes pins que SENSOR1 et SENSOR2. Pour gérer cela, des jumpers sont à disposition.

### Jumpers (brun)

Les jumpers permettent de choisir si l'on souhaite utiliser un bouton poussoir ou un capteur binaire.  Il suffit de placer le jumper à droite pour utiliser le bouton associé, et à gauche pour utiliser le capteur associé.

Le JUMPER1 permet de choisir entre BUTTON1 et SENSOR1, tandis que le JUMPER2 permet de choisir entre BUTTON2 et SENSOR2.

##### Entrées analogiques (rouge)

Ces bornes permettent de communiquer avec les pattes analogiques de l'Arduino Nano Every.  Il est aussi possible d'utiliser ces pattes comme des entrées/sorties digitales.  Il est cependant important de **ne pas** dépasser 5 V sur ces pins.

### Driver moteur (rose)

Le driver moteur est connecté à l'Arduino de la sorte :

- PWM1 = D10
- PWM2 = D9
- DIR1 = D11
- DIR2 = D8

Sa notice est disponible à cette adresse :

- https://www.mouser.be/pdfDocs/DRI0044_Web.pdf

### Connecteurs pour encodeurs (Bordeaux)

Ces connecteurs sont prévus pour des encodeurs en quadrature.  Ils ont une borne *+5V* pour alimenter l'encodeur' et deux bornes (*A* et B*) pour récupérer les signaux de l'encodeur.  

![](img/connec04.png)

Les connecteurs sont reliés à la carte Arduino de la sorte :

Ils sont câblés de la sorte :

- ENCODER1 A = D4
- ENCODER1 B = D5
- ENCODER2 A = D6
- ENCODER2 B = D7

*A* et B* étant connectés à des entrées/sorties binaires de l'Arduino, on peut aussi utiliser ces connecteurs pour d'autres capteurs, si on n'utilise pas d'encodeurs.

## Dimensions

![](img/cotes.png)

## Protection des entrées/sorties

Afin de protéger les entrées/sorties, des diodes Schottky (*D3* et *D4*) ont été placées entre la patte de l'Arduino et respectivement le +5V et la masse.  Cette solution permet de limiter la tension vue par la patte de l'Arduino entre 0V et 5 v.  
Une résistance de 330 $\Omega$ (*R2*) a été placée en série entre l patte de l'Arduino et la borne du connecteur, afin de limiter le courant dans les diodes.  
Cette résistance peut avoir un effet sur votre montage.  Par exemple, si vous voulez connecter une LED à une patte de l'Arduino, il vous faudra en tenir compte dans le calcul de la résistance de limitation du courant.

![](img/protection.png)

## Bill Of Materials (BOM)

La liste détaillée des composants se trouve dans le fichier [BoM.xlsx](/PCB/BoM.xlsx), dans le répertoire PCB.

## PCB Layout
>**Top Layout**
![](img/top_layout.png)

>**Bottom Layout**
![](img/bottom_layout.png)

## Schematic

>**Schematic KiCAD**
![](img/sch.png)
